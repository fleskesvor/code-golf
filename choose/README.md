Choose
======

Archived [description](https://web.archive.org/web/20131031172541/http://codegolf.com/choose) and [leaderboard](https://web.archive.org/web/20131101083306/http://codegolf.com/leaderboard/competition/choose)

*Leaderboard*

| Rank | User | Size | Language | Score |
| ---- | ---- | ---- | -------- | ----- |
| 1st | terjek | 35 | Perl | 10,000 (v8) |
| 2nd | tybalt89 | 35 | Perl | 10,000 (v3) |
| 3rd | o0lit3 | 35 | Perl | 10,000 (v18) |
| 4th | ySas | 35 | Perl | 10,000 (v3) |
| 5th | kounoike | 35 | Perl | 10,000 (v7) |
| 6th | difro | 35 | Perl | 10,000 (v15) |
| 7th | 0xF | 35 | Perl | 10,000 (v7) |
| 8th | primo | 35 | Perl | 10,000 (v8) |
| 9th | flagitious | 36 | Ruby | 9,722 (v13) |
| 10th | leonid | 36 | Ruby | 9,722 (v24) |
| ... | | | | |
| 67th | eivindkn | 46 | Perl | 7,608 (v16) |
