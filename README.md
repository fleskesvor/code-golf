Code Golf
=========

Code golf is a coding activity where the goal is to solve a challenge using the the least number of characters, or key strokes. Golfed code is obviously not of production quality, but doing well at code golf requires an extensive knowledge of the language you're using, and it's a great and fun way to explore the nooks and crannies of your language of choice.

Perl golf has been documented in [The Lighter Side of Perl Culture (Part IV)[http://www.perlmonks.org/?displaytype=print;node_id=437032], and the exercise was popular long before the now abandoned CodeGolf.com was launched. What was new with CodeGolf.com was that it offered an automated way to measure and rank solutions on leaderboard, and that it pitted practitioners of Perl, Python, PHP and Ruby against eachother.

Though that site is now long gone, there exists other similar web sites, like [Code Golf Stack Exchange](http://codegolf.stackexchange.com/) and [HackerRank](https://www.hackerrank.com/).


What's this then?
-----------------

The reason why I created this repository is because I want to try to collect my own submissions to CodeGolf.com in one place, and perhaps see if I can do better with Python, which is what I primarily use these days. Collecting the scripts might prove tricky though, since I wrote them long before I started using version control for my own hobby projects, and some of the scripts might have been stored on servers which no longer exist.

I was never anywhere close to the elite of golfers, but I'm pretty happy with having been within the top 100 at CodeGolf.com by the time it stopped working. Leaderboards have been preserved thanks to the [Internet Archive](https://web.archive.org/web/20140715000000*/http://codegolf.com/competition/overall_leaderboard/):

| Rank | User | Score |
| ---- | ---- | ----- |
| 1st | shinh | 249,669 (28 challenges) |
| 2nd | flagitious | 246,053 (28 challenges) |
| 3rd | ySas | 239,053 (27 challenges) |
| 4th | primo | 231,243 (25 challenges) |
| 5th | kounoike | 223,908 (27 challenges) |
| 6th | yvl | 215,399 (28 challenges) |
| 7th | 0xF | 214,810 (28 challenges) |
| 8th | ozy4dm | 202,770 (27 challenges) |
| 9th | Mark Byers | 198,515 (28 challenges) |
| 10th | hallvabo | 185,991 (28 challenges) |
| ... | | |
| 98th | eivindkn | 63,323 (11 challenges) |

(I'm 'eivindkn'.)

And here's the [Perl leaderboard](https://web.archive.org/web/20130222035628/http://codegolf.com/leaderboard/overall/perl):

| Rank | User | Score |
| ---- | ---- | ----- |
| 1st | ySas | 243,220 (27 challenges) |
| 2nd | kounoike | 235,167 (27 challenges) |
| 3rd | 0xF | 228,298 (28 challenges) |
| 4th | ott | 189,443 (24 challenges) |
| 5th | grizzley | 185,017 (27 challenges) |
| 6th | shinh | 172,118 (19 challenges) |
| 7th | o0lit3 | 163,449 (18 challenges) |
| 8th | Jasper | 158,217 (19 challenges) |
| 9th | szeryf | 154,323 (20 challenges) |
| 10th | terjek | 153,009 (17 challenges) |
| ... | | |
| 37th | eivindkn | 63,839 (11 challenges) |

The rules can also be found in the [Internet Archive](https://web.archive.org/web/20140306073858/http://codegolf.com/help).

