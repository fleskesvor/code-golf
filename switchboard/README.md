Switchboard
===========

Archived [description](https://web.archive.org/web/20130527172821/http://codegolf.com/switchboard) and [leaderboard](https://web.archive.org/web/20130221043450/http://codegolf.com/leaderboard/competition/switchboard/)

*Leaderboard*

| Rank | User | Size | Language | Score |
| ---- | ---- | ---- | -------- | ----- |
| 1st | tybalt89 | 56 | Perl | 10,000 (v2) |
| 2nd | robin | 56 | Perl | 10,000 (v5) |
| 3rd | kinaba | 58 | Perl | 9,655 (v15) |
| 4th | shinh | 59 | Perl | 9,491 (v19) |
| 5th | ersagun | 61 | Perl | 9,180 (v113) |
| 6th | ySas | 65 | Perl | 8,615 (v7) |
| 7th | Jasper | 65 | Perl | 8,615 (v6) |
| 8th | primo | 65 | Perl | 8,615 (v21) |
| 9th | kounoike | 66 | Perl | 8,484 (v20) |
| 10th | ott | 67 | Perl | 8,358 (v15) |
| ... | | | | |
64th | eivindkn | 103 | Perl | 5,436 (v6) |
