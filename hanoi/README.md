Tower of Hanoi
==============

Archived [description](https://web.archive.org/web/20131009094323/http://codegolf.com/tower-of-hanoi) and [leaderboard](https://web.archive.org/web/20130807215718/http://codegolf.com/leaderboard/competition/tower-of-hanoi/)

*Leaderboard*

| Rank | User | Size | Language | Score |
| ---- | ---- | ---- | -------- | ----- |
| 1st | primo | 103 | Ruby | 10,000 (v91) |
| 2nd | flagitious | 104 | Ruby | 9,903 (v28) |
| 3rd | shinh | 107 | Ruby | 9,626 (v31) |
| 4th | etcshadow | 110 | Perl | 9,363 (v12) |
| 5th | ySas | 115 | Perl | 8,956 (v31) |
| 6th | bearstearns | 115 | Perl | 8,956 (v16) |
| 7th | ott | 118 | Perl | 8,728 (v10) |
| 8th | gorash | 121 | Perl | 8,512 (v21) |
| 9th | Mark Byers | 121 | Ruby | 8,512 (v18) |
| 10th | kounoike | 127 | Perl | 8,110 (v41) |
| ... | | | | |
| 28th | eivindkn | 156 | Perl | 6,602 (v7) |
