Home on the Range
=================

Archived [description](https://web.archive.org/web/20130412061816/http://codegolf.com/home-on-the-range) and [leaderboard](https://web.archive.org/web/20110721234449/http://codegolf.com/leaderboard/competition/home-on-the-range/)

*Leaderboard*

| Rank | User | Size | Language | Score |
| ---- | ---- | ---- | -------- | ----- |
| 1st | Aidy | 50 | Perl | 10,000 (v12) |
| 2nd | dmo | 50 | Perl | 10,000 (v17) |
| 3rd | Jasper | 50 | Perl | 10,000 (v15) |
| 4th | o0lit3 | 52 | Perl | 9,615 (v33) |
| 5th | terjek | 53 | Perl | 9,433 (v10) |
| 6th | kinaba | 53 | Perl | 9,433 (v7) |
| 7th | ozy4dm | 53 | Perl | 9,433 (v25) |
| 8th | kounoike | 53 | Perl | 9,433 (v15) |
| 9th | szeryf | 53 | Perl | 9,433 (v6) |
| 10th | rhebus | 53 | Perl | 9,433 (v4) |
| ... | | | | |
| 236th | eivindkn | 137 | Perl | 3,649 (v1) |
