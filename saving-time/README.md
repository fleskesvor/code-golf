Saving Time
===========

Archived [description](https://web.archive.org/web/20131016220027/http://codegolf.com/saving-time) and [leaderboard](https://web.archive.org/web/20130528080408/http://codegolf.com/leaderboard/competition/saving-time)

*Leaderboard*

| Rank | User | Size | Language | Score |
| ---- | ---- | ---- | -------- | ----- |
| 1st | eyepopslikeamosquito | 93 | Perl | 10,000 (v102) |
| 2nd | Jasper | 96 | Perl | 9,687 (v28) |
| 3rd | o0lit3 | 96 | Perl | 9,687 (v2) |
| 4th | Grimy | 96 | Perl | 9,687 (v1) |
| 5th | ySas | 102 | Perl | 9,117 (v18) |
| 6th | shinh | 111 | Perl | 8,378 (v14) |
| 7th | leonid | 111 | Ruby | 8,378 (v26) |
| 8th | flagitious | 112 | Ruby | 8,303 (v7) |
| 9th | Mr.Rm | 114 | Perl | 8,157 (v3) |
| 10th | ozy4dm | 115 | Perl | 8,086 (v9) |
| ... | | | | |
| 141st | eivindkn | 193 | Perl | 4,818 (v2) |
