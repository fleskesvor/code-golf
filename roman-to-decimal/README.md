Roman to Decimal
================

Archived [description](https://web.archive.org/web/20130412064115/http://codegolf.com/roman-to-decimal) and [leaderboard](https://web.archive.org/web/20131029190746/http://codegolf.com/leaderboard/competition/roman-to-decimal)

*Leaderboard*

| Rank | User | Size | Language | Score |
| ---- | ---- | ---- | -------- | ----- |
| 1st | eyepopslikeamosquito | 53 | Ruby | 10,000 (v54) |
| 2nd | o0lit3 | 53 | Perl | 10,000 (v44) |
| 3rd | Grimy | 53 | Perl | 10,000 (v1) |
| 4th | primo | 55 | Ruby | 9,636 (v22) |
| 5th | flagitious | 56 | Ruby | 9,464 (v10) |
| 6th | seojay | 56 | Perl | 9,464 (v5) |
| 7th | ySas | 58 | Perl | 9,137 (v11) |
| 8th | leonid | 58 | Ruby | 9,137 (v24) |
| 9th | d310 | 58 | Perl | 9,137 (v5) |
| 10th | bearstearns | 59 | Perl | 8,983 (v24) |
| ... | | | | |
| 154th | eivindkn | 108 | Perl | 4,907 (v6) |