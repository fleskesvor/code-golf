Reverse
=======

Archived [description](https://web.archive.org/web/20131016224125/http://codegolf.com/reverse) and [leaderboard](https://web.archive.org/web/20130808213713/http://codegolf.com/leaderboard/competition/reverse/)

*Leaderboard*

| Rank | User | Size | Language | Score |
| ---- | ---- | ---- | -------- | ----- |
| 1st | kinaba | 51 | Ruby | 10,000 (v13) |
| 2nd | flagitious | 51 | Ruby | 10,000 (v27) |
| 3rd | shinh | 52 | Ruby | 9,807 (v14) |
| 4th | kounoike | 52 | Perl | 9,807 (v43) |
| 5th | primo | 55 | Perl | 9,272 (v48) |
| 6th | Jasper | 57 | Perl | 8,947 (v35) |
| 7th | yvl | 58 | Ruby | 8,793 (v23) |
| 8th | ySas | 58 | Perl | 8,793 (v36) |
| 9th | robin | 58 | Perl | 8,793 (v20) |
| 10th | leonid | 58 | Ruby | 8,793 (v51) |
| ... | | | | |
156th | eivindkn | 150 | Perl | 3,399 (v2) |
