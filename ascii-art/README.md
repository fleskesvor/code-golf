Bob Ross' The Joy of ASCII Art
==============================

Archived [description](https://web.archive.org/web/20130603004214/http://codegolf.com/bob-ross-the-joy-of-ascii-art) and [leaderboard](https://web.archive.org/web/20130222035617/http://codegolf.com/leaderboard/competition/bob-ross-the-joy-of-ascii-art/)

*Leaderboard*

| Rank | User | Size | Language | Score |
| ---- | ---- | ---- | -------- | ----- |
| 1st | Aidy | 50 | Perl | 10,000 (v8) |
| 2nd | kounoike | 51 | Perl | 9,803 (v5) |
| 3rd | tybalt89 | 51 | Perl | 9,803 (v2) |
| 4th | ySas | 51 | Perl | 9,803 (v6) |
| 5th | bass | 51 | Perl | 9,803 (v1) |
| 6th | robin | 51 | Perl | 9,803 (v5) |
| 7th | jojo | 51 | Perl | 9,803 (v8) |
| 8th | sparr | 51 | Perl | 9,803 (v20) |
| 9th | terjek | 51 | Perl | 9,803 (v6) |
| 10th | Ciaran | 51 | Perl | 9,803 (v27) |
| ... | | | | |
| 56th | eivindkn | 67 | Perl | 7,462 (v5) |
