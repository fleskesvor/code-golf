Grid Computing
==============

Archived [description](https://web.archive.org/web/20131028001511/http://codegolf.com/grid-computing) and [leaderboard](https://web.archive.org/web/20120824234302/http://codegolf.com/leaderboard/competition/grid-computing)

*Leaderboard*

| Rank | User | Size | Language | Score |
| ---- | ---- | ---- | -------- | ----- |
| 1st | tybalt89 | 43 | Perl | 10,000 (v6) |
| 2nd | eyepopslikeamosquito | 43 | Perl | 10,000 (v11) |
| 3rd | Jasper | 43 | Perl | 10,000 (v37) |
| 4th | kounoike | 44 | Perl | 9,772 (v13) |
| 5th | o0lit3 | 44 | Perl | 9,772 (v46) |
| 6th | Aidy | 45 | Perl | 9,555 (v23) |
| 7th | pung96 | 46 | Perl | 9,347 (v14) |
| 8th | terjek | 46 | Perl | 9,347 (v8) |
| 9th | shinh | 46 | Perl | 9,347 (v20) |
| 10th | difro | 47 | Perl | 9,148 (v22) |
| ... | | | | |
| 89th | eivindkn | 71 | Perl | 6,056 (v6) |
